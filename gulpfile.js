'use strict';

let gulp = require('gulp');
let sass = require('gulp-sass');
let concat = require('gulp-concat');
// let uglifycss = require('gulp-uglifycss');

sass.compiler = require('node-sass');

const SASS_DEST = './app/sass/*.scss'
const STYLE_DEST = './dist/'

gulp.task('compileConcatAndUglify', function () {
    return gulp.src(SASS_DEST)
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        // .pipe(uglifycss({
        //     "maxLineLen": 80,
        //     "uglyComments": true
        // }))
        .pipe(gulp.dest(STYLE_DEST));
});

gulp.task('watch', function () {
    gulp.watch(SASS_DEST, gulp.series('compileConcatAndUglify'));
});

gulp.task('default', gulp.series('compileConcatAndUglify', 'watch'))